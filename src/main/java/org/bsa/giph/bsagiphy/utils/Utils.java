package org.bsa.giph.bsagiphy.utils;

import org.bsa.giph.bsagiphy.exceptions.IncorrectDataException;
import org.springframework.stereotype.Component;

@Component
public class Utils {
    private String FILE_NAME_PATTERN = "^[A-z0-9\\-\\_]+$";

    public void validate(String name0, String... names) {

        if (!name0.matches(FILE_NAME_PATTERN)) {
            throw new IncorrectDataException("Validate Error: " + name0);
        }

        for (String name : names) {
            if (!name0.matches(FILE_NAME_PATTERN)) {
                throw new IncorrectDataException("Validate Error: " + name);
            }
        }
    }
}

