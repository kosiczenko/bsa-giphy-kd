package org.bsa.giph.bsagiphy.filter;

import org.bsa.giph.bsagiphy.exceptions.IncorrectDataException;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class FilterByValuePresent implements Filter {
    private final String PARAM_NAME = "X-BSA-GIPHY";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String param = request.getHeader(PARAM_NAME);
        if (param == null) {
            throw new IncorrectDataException("Parameter " + PARAM_NAME + " not found in header.");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {}
}
