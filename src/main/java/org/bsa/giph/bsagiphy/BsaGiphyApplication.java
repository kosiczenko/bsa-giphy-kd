package org.bsa.giph.bsagiphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsaGiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsaGiphyApplication.class, args);
    }

}
