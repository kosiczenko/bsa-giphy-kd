package org.bsa.giph.bsagiphy.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class History {
    LocalDate date;
    String query;
    String gif;
}
