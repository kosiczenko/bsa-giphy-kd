package org.bsa.giph.bsagiphy.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cache {
    private String query;
    private String[] gifs;
}
