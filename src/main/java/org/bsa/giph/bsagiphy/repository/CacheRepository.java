package org.bsa.giph.bsagiphy.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.bsa.giph.bsagiphy.exceptions.DataNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class CacheRepository {
    @Value("${cache.path}")
    private String PATH = "";

    public void clearCache() {
        // TODO Очистить кэш на диске
        try {
            FileUtils.deleteDirectory(new File(PATH));
        } catch (IOException e) {
            throw new DataNotFoundException("Cache folder not found");
        }
    }

    public List<Map<String, Object>> getAllCacheByQuery(String query) {
//        Stream<String> paths = Optional
//                .of(new File(PATH + query))
//                .map(File::listFiles).map(Arrays::asList)
//                .orElseThrow(() -> new DataNotFoundException("Folder not found: " + query))
//                .stream().filter(File::isFile).map(File::getName)
        try {

            List<Map<String, Object>> list = new ArrayList<>();
            list.add(new HashMap<>(Map.of("query", query)));

            list.get(0).put("gifs", Files.walk(Paths.get(PATH + query))
                    .filter(Files::isRegularFile)
                    .map(Path::normalize)
                    .map(Path::toString)
                    .collect(Collectors.toList()));

            return list;
        } catch (IOException e) {
            throw new DataNotFoundException("Folder not found");
        }
    }
}
