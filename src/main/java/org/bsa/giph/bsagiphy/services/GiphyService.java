package org.bsa.giph.bsagiphy.services;

import org.bsa.giph.bsagiphy.model.dto.GifFile;
import org.bsa.giph.bsagiphy.model.dto.Query;
import org.bsa.giph.bsagiphy.model.entities.GifEntity;
import org.bsa.giph.bsagiphy.repository.CacheRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
public class GiphyService {

    private final String SEARCH_URL;
    private final String API_KEY;
    private CacheRepository cacheRepository;

    @Autowired
    public void setCacheRepository(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public GiphyService(@Value("${giphy.url}")String SEARCH_URL,
                        @Value("${api.key}") String API_KEY) {
        this.SEARCH_URL = SEARCH_URL;
        this.API_KEY = API_KEY;
    }

    public GifEntity searchGif(String userId, Query query) {
        UriComponentsBuilder uriComponentsBuilder =
                    UriComponentsBuilder.fromHttpUrl(SEARCH_URL)
                                        .cloneBuilder()
                                        .queryParam("api_key", API_KEY)
                                        .queryParam("tag", query.getQuery())
                                        .queryParam("random_id", userId);

        RestTemplate restTemplate = new RestTemplate();
        GifFile gifFile = restTemplate.getForObject(uriComponentsBuilder.toUriString(),
                                                    GifFile.class);

        JSONObject jsonObject = new JSONObject(gifFile).getJSONObject("data");

        StringBuilder url = new StringBuilder(jsonObject.getJSONObject("images").getJSONObject("downsized").getString("url"));
        url.replace(8, 14, "i");

        return new GifEntity(jsonObject.getString("id"),
                url.toString(),
                query.getQuery());
    }

    public List<?> getAllGifs() {
        return cacheRepository.getAllCacheByQuery("");
    }
}
