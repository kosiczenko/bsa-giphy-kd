package org.bsa.giph.bsagiphy.services;

import org.bsa.giph.bsagiphy.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CacheService {

    private CacheRepository cacheRepository;

    @Autowired
    public void setCacheRepository(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public List<Map<String, Object>> getAllCache(Optional<String> query) {
       return  null;
    }

    public List<Map<String, Object>> generateCache(String query) {
        return  null;
    }

    public void clearCache() {
        cacheRepository.clearCache();
    }


}
