package org.bsa.giph.bsagiphy.services;

import org.bsa.giph.bsagiphy.model.entities.GifEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Service
public class LocalStorageService {
    @Value("${cache.path}")
    private String PATH;

    public File downloadGifByUrl(GifEntity gifEntity) {
        File folder = findOrCreateFolder(PATH+gifEntity.getQuery());
        File gif = new File(folder, gifEntity.getId() + ".gif");

        try (BufferedInputStream inputStream =
                     new BufferedInputStream(new URL(gifEntity.getUrl()).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(gif);
        ) {
            int bytesRead;
            byte[] buffer = new byte[1024];
            do {
                bytesRead = inputStream.read(buffer, 0, 1024);
                if (bytesRead > 0) {
                    fileOutputStream.write(buffer);
                } else {
                    break;
                }
            } while (bytesRead != -1);
            return gif;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public File downloadGifByQuery(String query) {

        return null;
    }

    private File findOrCreateFolder(String folderName) {
        File folder = new File(folderName);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
    }

    public Map<String, File[]> getFullCache(String query, String folder) {
        int counter = 0;
        File genFile = new File(PATH + folder);
        if (query != null) {
            File[] files = getCacheByQuery(query);
            var map = new HashMap<String, File[]>();
            map.put(query, files);
            return map;
        } else {
            var map = new HashMap<String, File[]>();
            File[] childs = new File[genFile.listFiles().length];
            for (File gif : genFile.listFiles()) {
                childs[counter] = gif;
                counter++;
            }

            map.put("gifs", childs);
            return map;
        }
    }

    public File[] getCacheByQuery(String query) {
        var file = new File(PATH + query);
        File[] files = file.listFiles();
        return files;
    }
}
