package org.bsa.giph.bsagiphy.contollers;

import lombok.extern.slf4j.Slf4j;
import org.bsa.giph.bsagiphy.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/user/")
public class UserController {

    private Utils utils;

    @Autowired
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    @GetMapping("{user_id}/all")
    public ResponseEntity<?> getAllGifsByUserId(@PathVariable String user_id) {
        log.info("GET /user/:id/all =>  getAllGifsByUserId(user_id) " + user_id);
        // TODO: Получить список всех файлов с папки пользователя на диске
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping("{user_id}/history")
    public ResponseEntity<?> getHistoryByUserId(@PathVariable String user_id) {
        log.info("GET /user/:id/history => getHistoryByUserId(user_id) " + user_id);
        // TODO: Получить историю пользователя
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @DeleteMapping("{user_id}/history/clean")
    public ResponseEntity<?> deleteHistoryByUserId(@PathVariable String user_id) {
        log.info("GET DELETE /user/:id/history/clean => deleteHistoryByUserId(user_id) " + user_id);
        // TODO: Очистить историю пользователя
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping("{user_id}/search")
    public ResponseEntity<?> searchGif(@PathVariable String user_id,
                                       @RequestParam(required = false) String query,
                                       @RequestParam(required = false) String force) {

        log.info("GET /user/:id/search?query&force => searchGif() user_id " + user_id);
        log.info("query =" + query);
        log.info("force =" + force);

        utils.validate(query);

        if(force == null) {
            // TODO Выполнить поиск картинки.
        } else {
            // TODO Если указан параметр force, то игнорировать кэш в памяти
            //  и сразу читать данные с диска. Добавить файл в кэш в памяти, если его еще там нет.
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("{user_id}/generate")
    public ResponseEntity<?> generateGif(@PathVariable String user_id,
                                         @RequestParam String query,
                                         @RequestParam String force) {
        log.info("POST /user/:id/generate => generateGif()");
        log.info("user_id " + user_id);
        log.info("query " + query);
        log.info("force " + query);

        utils.validate(user_id, query);
        // TODO Сгенерировать GIF.
        if(force != null) {
            // TODO Если указан параметр force, то игнорировать кэш на диске
            //  (папка cache) и сразу искать файл в giphy.com. Добавить файл в кэш на диске
            //  (папка cache). (Доп. задание: избежать дублирования GIF файлов в кэше)
        } else {
            // TODO
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @DeleteMapping("{user_id}/reset")
    public ResponseEntity<?> clearCacheInMemory(@PathVariable String user_id, @RequestBody String query) {
        log.info("DELETE /user/:id/reset?query => clearCacheInMemory()");
        log.info("user_id " + user_id);
        log.info("query " + query);
        // TODO Очистить кэш пользователя в памяти по ключу query. Если ключ не указан,
        //  то очистить все данные по пользователю в кэше в памяти.
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @DeleteMapping("{user_id}/clean")
    public ResponseEntity<?> clearAllCache(@PathVariable String user_id) {
        log.info("DELETE /user/:id/clean => clearAllCache()");
        log.info("user_id " + user_id);
        // TODO Удалить все данные по пользователю как на диске так и в кэше в памяти
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
