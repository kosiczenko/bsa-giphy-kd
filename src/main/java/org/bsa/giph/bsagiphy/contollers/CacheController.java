package org.bsa.giph.bsagiphy.contollers;

import lombok.extern.slf4j.Slf4j;
import org.bsa.giph.bsagiphy.model.dto.Cache;
import org.bsa.giph.bsagiphy.model.dto.Query;
import org.bsa.giph.bsagiphy.services.CacheService;
import org.bsa.giph.bsagiphy.services.GiphyService;
import org.bsa.giph.bsagiphy.services.LocalStorageService;
import org.bsa.giph.bsagiphy.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/cache")
public class CacheController {

    private Utils utils;
    private CacheService cacheService;
    private LocalStorageService localStorageService;
    private GiphyService giphyService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Autowired
    public void setLocalStorageService(LocalStorageService localStorageService) {
        this.localStorageService = localStorageService;
    }

    @Autowired
    public void setGiphyService(GiphyService giphyService) {
        this.giphyService = giphyService;
    }

    @Autowired
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    @GetMapping("/")
    public ResponseEntity<?> getCacheByQuery(@RequestParam(required = false) String query) {
        log.info("GET /cache?query => getCacheByQuery(query) " + query);
        utils.validate(query);

        // TODO Получить кэш с диска. Если указан query, то выбрать только соответствующие файлы.
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("/generate")
    public ResponseEntity<?> generateGif(@RequestParam(required = false) String query) {//
        log.info("POST /cache/generate =>  generateGif()" + query);
        utils.validate(query);

        // TODO Скачать картинку из giphy.com и положить в соответствующую папку в кэше на диске.
        var gif = giphyService.searchGif(null ,new Query(query, true));
        localStorageService.downloadGifByUrl(gif);
        // Вернуть объект со всеми картинками в кэше на диске.

        Cache cache = new Cache();
        return new ResponseEntity<>(cache, HttpStatus.OK);
    }

    @DeleteMapping("/cache")
    public ResponseEntity<?> clearCache() {
        log.info("DELETE /cache => clearCache()");
        cacheService.clearCache();
        return ResponseEntity.ok().build();
    }
}
