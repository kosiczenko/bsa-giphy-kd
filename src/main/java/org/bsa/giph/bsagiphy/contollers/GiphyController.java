package org.bsa.giph.bsagiphy.contollers;

import lombok.extern.slf4j.Slf4j;
import org.bsa.giph.bsagiphy.services.GiphyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class GiphyController {

    public GiphyService giphyService;

    @GetMapping("/gifs")
    public ResponseEntity<?> getAllGifs() {
        log.info("GET /gifs =>  getAllGifs()");
        // TODO Получить список всех файлов без привязки к ключевым словам
        List allGifs = giphyService.getAllGifs();
        return new ResponseEntity<>(allGifs, HttpStatus.OK);
    }
}
